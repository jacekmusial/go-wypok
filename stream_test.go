package go_wypok

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWykopHandlerGetStreamLast6HoursHotEntries(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	entries, wypokError := wh.GetStreamLast6HoursHotEntries(0)
	checkGetStreamFunctionResult(t, entries, wypokError)
}

func TestWykopHandlerGetStreamLast12HoursHotEntries(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	entries, wypokError := wh.GetStreamLast12HoursHotEntries(0)
	checkGetStreamFunctionResult(t, entries, wypokError)
}

func TestWykopHandlerGetStreamLast24HoursHotEntries(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	entries, wypokError := wh.GetStreamLast24HoursHotEntries(0)
	checkGetStreamFunctionResult(t, entries, wypokError)
}

func TestWykopHandlerGetStreamEntries(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	entries, wypokError := wh.GetStreamEntries(0)
	checkGetStreamFunctionResult(t, entries, wypokError)
}

func checkGetStreamFunctionResult(t *testing.T, entries []Entry, wError *WykopError) {
	assert.Nil(t, wError, "Expected that error will be nil")
	assert.NotEmpty(t, entries, "Expected that entries list won't be empty. It's impossible!")
}

type buildStreamURLTestCaseHelper struct {
	actual, expected string
}

func TestBuildingStreamURLsNotLoggedIn(t *testing.T) {
	wh = WykopHandler{
		appKey: "AAA",
	}

	testCases := []buildStreamURLTestCaseHelper{
		buildStreamURLTestCaseHelper{
			getStreamIndexUrl(&wh, 0),
			"https://a.wykop.pl/stream/index/appkey/AAA/page/0",
		},
		buildStreamURLTestCaseHelper{
			getStreamHotUrl(&wh, 0, 1),
			"https://a.wykop.pl/stream/hot/appkey/AAA/page/0/period/1",
		},
	}

	for _, testCase := range testCases {
		assert.Equal(t, testCase.expected, testCase.actual)
	}
}

func TestBuildingStreamURLsLoggedIn(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	wh.LoginToWypok()

	testCases := []buildStreamURLTestCaseHelper{
		buildStreamURLTestCaseHelper{
			getStreamIndexUrl(&wh, 0),
			fmt.Sprintf("https://a.wykop.pl/stream/index/userkey/%s/appkey/%s/page/0", wh.authResponse.Userkey, wh.appKey),
		},
		buildStreamURLTestCaseHelper{
			getStreamHotUrl(&wh, 0, 1),
			fmt.Sprintf("https://a.wykop.pl/stream/hot/userkey/%s/appkey/%s/page/0/period/1", wh.authResponse.Userkey, wh.appKey),
		},
	}

	for _, testCase := range testCases {
		assert.Equal(t, testCase.expected, testCase.actual)
	}
}
