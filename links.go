package go_wypok

import "fmt"

type Link struct {
	ID              uint
	Title           string
	Description     string
	Tags            string
	Url             string
	SourceUrl       string        `json:"source_url"`
	VoteCount       int           `json:"vote_count"`
	CommentCount    int           `json:"comment_count"`
	ReportCount     int           `json:"report_count"`
	Date            WypokShitDate `json:"date"`
	Author          string
	AuthorAvatar    string `json:"avatar_avatar"`
	AuthorAvatarMed string `json:"avatar_med"`
	AuthorAvatarLo  string `json:"avatar_lo"`
	AuthorGroup     int    `json:"author_group"`
	AuthorSex       string `json:"author_sex"`
	Preview         string
	UserLists       []int `json:"user_lists"`
	Plus18          bool  `json:"plus_18"`
	Status          string
	CanVote         bool `json:"can_vote"`
	IsHot           bool `json:"is_hot"`
	HasOwnContent   bool `json:"has_own_content"`
	Category        string
	CategoryName    string            `json:"category_name,omitempty"`
	UserVote        WykopShitUserVote `json:"user_vote,omitempty"`
	UserObserve     bool              `json:"user_observe,omitempty"`
	UserFavorite    bool              `json:"user_favorite,omitempty"`
}

func (wh *WykopHandler) GetMainPageLinks(page uint) (mainPageLinks []Link, wypokError *WykopError) {
	urlAddress := getMainPageUrl(wh, page)
	wypokError = wh.sendRequestAndReturnStruct(urlAddress, &mainPageLinks)
	return
}

func (wh *WykopHandler) GetUpcomingLinks(page uint) (mainPageLinks []Link, wypokError *WykopError) {
	urlAddress := getUpcomingPageUrl(wh, page)
	wypokError = wh.sendRequestAndReturnStruct(urlAddress, &mainPageLinks)
	return
}

func (wh *WykopHandler) BuryLink(id uint) (returned [2]int, wypokError *WykopError) {
	urlAddress := getBuryUrl(wh, id)
	wypokError = wh.sendRequestAndReturnStruct(urlAddress, returned)
	return
}

func getBuryUrl(wh *WykopHandler, id uint) string {
	return fmt.Sprintf(LINK_BURY, id, wh.appKey, wh.authResponse.Userkey)
}

func getMainPageUrl(wh *WykopHandler, page uint) string {
	if wh.authResponse.Userkey != "" {
		return fmt.Sprintf(MAIN_PAGE_LOGGEDIN, wh.authResponse.Userkey, wh.appKey, page)
	}
	return fmt.Sprintf(MAIN_PAGE, wh.appKey, page)
}

func getUpcomingPageUrl(wh *WykopHandler, page uint) string {
	if wh.authResponse.Userkey != "" {
		return fmt.Sprintf(UPCOMING_PAGE_LOGGEDIN, wh.authResponse.Userkey, wh.appKey, page)
	}
	return fmt.Sprintf(UPCOMING_PAGE, wh.appKey, page)
}
