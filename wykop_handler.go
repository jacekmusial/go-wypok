package go_wypok

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"net/url"

	"github.com/parnurzeal/gorequest"
)

const (
	contentType       = "Content-Type"
	mediaTypeFormType = "application/x-www-form-urlencoded"
	apiSignHeader     = "apisign"
	accountKeyHeader  = "accountkey"
)

type WykopHandler struct {
	appKey        string
	authResponse  AuthenticationResponse
	connectionKey string
	secret        string
}

type AuthenticationResponse struct {
	Login        string
	Email        string
	ViolationUrl string `json:"violation_url"`
	Userkey      string
}

func (wh *WykopHandler) LoginToWypok() *WykopError {

	responseBody := wh.sendPostRequestForBody(getLoginUrl(wh))
	wh.authResponse = AuthenticationResponse{}

	return wh.getObjectFromJson(responseBody, &wh.authResponse)
}

func (wh *WykopHandler) sendPostRequestForBody(address string) string {
	body := url.Values{}
	body.Add(accountKeyHeader, wh.connectionKey)

	_, bodyResp, _ := gorequest.New().Post(address).
		Set(contentType, mediaTypeFormType).
		Set(apiSignHeader, wh.hashRequest(address+wh.connectionKey)).
		Send(body).
		End()

	return bodyResp
}

func (wh *WykopHandler) sendRequestAndReturnStruct(urlAddress string, target interface{}) (wypokError *WykopError) {
	responseBody := wh.sendPostRequestForBody(urlAddress)
	wypokError = wh.getObjectFromJson(responseBody, &target)
	return
}

func (wh *WykopHandler) getObjectFromJson(bodyResponse string, target interface{}) (wypokError *WykopError) {
	b := []byte(bodyResponse)
	if err := json.Unmarshal(b, &wypokError); err != nil {
		// failed to unmarshall error, this is kinda ok, means that API worked
	}

	if wypokError != nil && wypokError.ErrorObject.Message != "" {
		return wypokError
	}

	// if wypokError.ErrorObject.Message != "" {
	// 	wypokError = new(WykopError)
	// 	wypokError.ErrorObject.Message = "Coś poszło nie tak, wykop api nie zwróciło ani błędu ani obiektu"
	// 	wypokError.ErrorObject.Code = -1
	// 	return wypokError
	// }

	if targetError := json.Unmarshal(b, target); targetError != nil {
		// this might happen when wypok is being ddosed/updated or Kiner is parting hard in the server room
		// this might happen when a.wykop.pl returned html, or empty response, shit happens.
		wypokError = new(WykopError)
		wypokError.ErrorObject.Message = "Coś poszło nie tak, wykop api nie zwróciło ani błędu ani obiektu"
		wypokError.ErrorObject.Code = -1
		return wypokError
	}
	return nil
}

func (wh *WykopHandler) hashRequest(address string) string {
	toHash := wh.secret + address
	mString := []byte(toHash)
	hash := md5.Sum([]byte(mString))
	return hex.EncodeToString(hash[:])
}

func (wh *WykopHandler) SetAppKey(appKey string) {
	wh.appKey = appKey
}

func (wh *WykopHandler) SetSecret(secret string) {
	wh.secret = secret
}

func (wh *WykopHandler) SetConnectionKey(connectionKey string) {
	wh.connectionKey = connectionKey
}
