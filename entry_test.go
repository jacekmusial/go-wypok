package go_wypok

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	embed = "https://golang.org/doc/gopher/frontpage.png"
)

func TestBuildingUrls(t *testing.T) {
	expectedGetEntryUrl := "https://a.wykop.pl/entries/index/999/appkey/APPKEY"
	expectedAddEntryUrl := "https://a.wykop.pl/entries/add/appkey/APPKEY/userkey/USERKEY"
	expectedEditEntryUrl := "https://a.wykop.pl/entries/edit/999/appkey/APPKEY/userkey/USERKEY"
	expectedDelEntryUrl := "https://a.wykop.pl/entries/delete/999/appkey/APPKEY/userkey/USERKEY"

	expectedAddEntryCommentUrl := "https://a.wykop.pl/entries/addComment/999/appkey/APPKEY/userkey/USERKEY"
	expectedEditEntryCommentUrl := "https://a.wykop.pl/entries/editComment/999/666/appkey/APPKEY/userkey/USERKEY"
	expectedDelEntryCommentUrl := "https://a.wykop.pl/entries/deleteComment/999/666/appkey/APPKEY/userkey/USERKEY"

	expectedEntryVoteUrl := "https://a.wykop.pl/entries/vote/entry/999/appkey/APPKEY/userkey/USERKEY"
	expectedEntryUnvoteUrl := "https://a.wykop.pl/entries/unvote/entry/999/appkey/APPKEY/userkey/USERKEY"

	expectedEntryCommentVoteUrl := "https://a.wykop.pl/entries/vote/comment/999/666/appkey/APPKEY/userkey/USERKEY"
	expectedEntryCommentUnoteUrl := "https://a.wykop.pl/entries/unvote/comment/999/666/appkey/APPKEY/userkey/USERKEY"

	expectedEntryFavoriteUrl := "https://a.wykop.pl/entries/favorite/999/appkey/APPKEY/userkey/USERKEY"

	appKey := "APPKEY"
	entryId := uint(999)
	commentId := uint(666)
	userKey := "USERKEY"
	wh := new(WykopHandler)
	wh.appKey = appKey
	wh.authResponse.Userkey = userKey

	assert.Equal(t, expectedGetEntryUrl, getEntryUrl(entryId, appKey))
	assert.Equal(t, expectedAddEntryUrl, getAddEntryUrl(wh))
	assert.Equal(t, expectedEditEntryUrl, getEditEntryUrl(entryId, wh))
	assert.Equal(t, expectedDelEntryUrl, getDeleteEntryUrl(entryId, wh))
	assert.Equal(t, expectedAddEntryCommentUrl, getEntryAddCommentUrl(entryId, wh))
	assert.Equal(t, expectedEditEntryCommentUrl, getEditEntryCommentUrl(entryId, commentId, wh))
	assert.Equal(t, expectedDelEntryCommentUrl, getDeleteCommentUrl(entryId, commentId, wh))
	assert.Equal(t, expectedEntryVoteUrl, getEntryVoteUrl(entry, entryId, commentId, wh))
	assert.Equal(t, expectedEntryUnvoteUrl, getEntryUnvoteUrl(entry, entryId, commentId, wh))
	assert.Equal(t, expectedEntryCommentVoteUrl, getEntryVoteUrl(comment, entryId, commentId, wh))
	assert.Equal(t, expectedEntryCommentUnoteUrl, getEntryUnvoteUrl(comment, entryId, commentId, wh))
	assert.Equal(t, expectedEntryFavoriteUrl, getEntryFavoriteUrl(entryId, wh))
}

func TestWykopHandlerGetEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	entry, wypokError := wh.GetEntry(0)
	assert.NotNil(t, wypokError)
	assert.Equal(t, 2, wypokError.ErrorObject.Code)
	assert.Equal(t, "Niepoprawne parametry", wypokError.ErrorObject.Message)

	assert.Equal(t, uint(0), entry.ID)
	assert.Empty(t, entry.Author)
}

func TestWykopHandlerGetCorrectEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	entry, wypokError := wh.GetEntry(23391703)
	assert.Nil(t, wypokError)

	assert.Equal(t, uint(23391703), entry.ID)
	assert.NotEmpty(t, entry.Author)
}

func TestWykopHandlerPostEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	newEntry, wypokError := wh.PostEntry("TODO: wstaw tresc wpisu")

	assert.Nil(t, wypokError)
	assert.NotEmpty(t, newEntry.ID)

	entry, getEntryError := wh.GetEntry(newEntry.ID)
	assert.Nil(t, getEntryError)
	assert.NotEmpty(t, entry.Author)

	assert.Equal(t, entry.ID, newEntry.ID)

	deleteEntryResponse, deleteEntryError := wh.DeleteEntry(newEntry.ID)
	assert.Nil(t, deleteEntryError, "Nie oczekiwano wypok error")

	assert.Equal(t, newEntry.ID, deleteEntryResponse.ID, "ID usunietego wpisu i ID wpisy ktory chcialem usunac sie nie zgadzaja")
}

func TestGettingEntriesFromTag(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	entriesFromTagPage1, err := wh.GetEntriesFromTag("interfacesmieci", 1)
	assert.Nil(t, err)
	assert.NotEmpty(t, entriesFromTagPage1)

	entriesFromTagPage2, err2 := wh.GetEntriesFromTag("interfacesmieci", 2)
	assert.Nil(t, err2)
	assert.NotEmpty(t, entriesFromTagPage2)

	assert.NotEqual(t, entriesFromTagPage1.Items[0].Url, entriesFromTagPage2.Items[0].Url)
}

func TestWykopHandlerPostEntryWithEmbeddedContent(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	content := "Test"

	response, wykopError := wh.PostEntryWithEmbeddedContent(content, embed)
	assert.Nil(t, wykopError)
	assert.NotNil(t, response)
	assert.NotNil(t, response.ID)

	newEntry, newEntryError := wh.GetEntry(response.ID)
	assert.Nil(t, newEntryError)
	assert.NotEmpty(t, newEntry.Embed.Url)

	deleteResponse, deleteResponseError := wh.DeleteEntry(response.ID)
	assert.Nil(t, deleteResponseError, "Expected no error deleting entry")
	assert.NotNil(t, deleteResponse)

	assert.Equal(t, response.ID, deleteResponse.ID)
}

//func TestUploadingEntryWithImage(t *testing.T) {
//	teardownTestCase := setupTestCase(t)
//	defer teardownTestCase(t)
//	wh.LoginToWypok()
//
//	entryBody := "test"
//
//	entryResponse, wypokError := wh.PostEntryWithImage(entryBody, "/home/agilob/Pictures/penguin_wings.jpg")
//	assert.Nil(t, wypokError)
//	assert.NotNil(t, entryResponse.ID)
//
//	entryId, _ := strconv.Atoi(entryResponse.ID)
//	entry, errorGettingEntry := wh.GetEntry(entryId)
//	assert.Nil(t, errorGettingEntry, "Expected no error getting entry that was created before")
//	assert.Equal(t, entryBody, entry.Body, "Message body is not what was submitted")
//
//	// assert here that entry.Embed is populated
//
//	deleteResponse, deleteResponseError := wh.DeleteEntry(entryId)
//	assert.Nil(t, deleteResponseError, "Expected no error deleting entry")
//	assert.Equal(t, entryResponse.ID, deleteResponse.ID)
//}

func TestWykopHandlerDeleteEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	newEntry, wypokError := wh.PostEntry("test deleting entry")
	assert.Nil(t, wypokError)
	assert.NotEmpty(t, newEntry.ID)

	deleteEntryResponse, deleteEntryError := wh.DeleteEntry(newEntry.ID)
	assert.Nil(t, deleteEntryError)
	assert.Equal(t, deleteEntryResponse.ID, newEntry.ID)
}

func TestWykopHandlerAddEntryComment(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	newEntryResponse, wypokError := wh.PostEntry("TODO: wstaw tresc wpisu")
	assert.Nil(t, wypokError)
	assert.NotEmpty(t, newEntryResponse.ID)

	comment := "TODO: wstaw tresc komentarza"

	firstCommentResponse, firstCommentError := wh.AddEntryComment(newEntryResponse.ID, comment)
	assert.Nil(t, firstCommentError)
	assert.NotEmpty(t, firstCommentResponse.ID)

	commentWithEmbedResp, commentWithEmbeddError := wh.AddEntryCommentWithEmbeddedContent(newEntryResponse.ID, comment, embed)
	assert.Nil(t, commentWithEmbeddError)
	assert.NotEmpty(t, commentWithEmbedResp.ID)

	newEntry, newEntryError := wh.GetEntry(newEntryResponse.ID)
	assert.Nil(t, newEntryError)
	assert.Len(t, newEntry.Comments, 2)
	assert.NotEmpty(t, newEntry.Comments[1].Embed.Url)

	delEntryResponse, delEntryError := wh.DeleteEntryComment(newEntryResponse.ID, firstCommentResponse.ID)
	assert.Nil(t, delEntryError)
	assert.Equal(t, firstCommentResponse.ID, delEntryResponse.ID)

	deleteEntryResponse, deleteEntryError := wh.DeleteEntry(newEntryResponse.ID)
	assert.Nil(t, deleteEntryError)
	assert.Equal(t, deleteEntryResponse.ID, newEntryResponse.ID)
}

func TestWykopHandlerEditEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	newEntry, wypokError := wh.PostEntry("TODO: wstaw tresc wpisu")
	assert.Nil(t, wypokError)
	assert.NotEmpty(t, newEntry.ID)

	newEntryContent := "new entry content"

	editEntryResponse, editEntryError := wh.EditEntry(newEntry.ID, newEntryContent)
	assert.Nil(t, editEntryError)
	assert.NotEmpty(t, editEntryResponse.ID)

	editedEntry, editedEntryError := wh.GetEntry(newEntry.ID)
	assert.Nil(t, editedEntryError)
	assert.NotEmpty(t, editEntryResponse.ID)
	assert.Equal(t, newEntryContent, editedEntry.Body)

	deleteEntryResponse, deleteEntryError := wh.DeleteEntry(newEntry.ID)
	assert.Nil(t, deleteEntryError)
	assert.Equal(t, deleteEntryResponse.ID, newEntry.ID)
}

func TestWykopHandlerEditEntryComment(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	newEntry, wypokError := wh.PostEntry("TODO: wstaw tresc wpisu")
	assert.Nil(t, wypokError)
	assert.NotEmpty(t, newEntry.ID)

	newCommentContent := "comment content"

	newComment, addCommentError := wh.AddEntryComment(newEntry.ID, "TODO: wstaw tresc komentarza")
	assert.Nil(t, addCommentError)
	assert.NotEmpty(t, newComment.ID)

	editCommentResponse, editCommentError := wh.EditEntryComment(newEntry.ID, newComment.ID, newCommentContent)
	assert.Nil(t, editCommentError)
	assert.NotEmpty(t, editCommentResponse.ID)

	entry, entryError := wh.GetEntry(newEntry.ID)
	assert.Nil(t, entryError)
	assert.NotEmpty(t, entry.ID)
	assert.Equal(t, newCommentContent, entry.Comments[0].Body)

	deleteEntryResponse, deleteEntryError := wh.DeleteEntry(newEntry.ID)
	assert.Nil(t, deleteEntryError)
	assert.Equal(t, deleteEntryResponse.ID, newEntry.ID)
}

func TestWykopHandlerUpvoteEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	voteResponse, voteError := wh.UpvoteEntry(27336289)
	assert.Nil(t, voteError)
	assert.True(t, voteResponse.Vote > 0)
}

func TestWykopHandlerUnvoteEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	voteResponse, voteError := wh.UnvoteEntry(27336289)
	assert.Nil(t, voteError)
	assert.True(t, voteResponse.Vote == 0, "This might fail and this is ok.")
}

func TestWykopHandlerFavoriteEntry(t *testing.T) {
	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)
	wh.LoginToWypok()

	// add to favorite
	favResponse, favResponseError := wh.FavoriteEntry(27336289)
	assert.Nil(t, favResponseError)
	assert.True(t, favResponse.UserFavorite)

	// re-add to favorite = unfavorite
	unfavResponse, unfavResponseError := wh.FavoriteEntry(27336289)
	assert.Nil(t, unfavResponseError)
	assert.False(t, unfavResponse.UserFavorite)
}
